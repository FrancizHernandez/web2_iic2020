const express = require('express');
const app = express();
const cors = require("cors");


app.get('/', function (req, res) {
  res.send('Saludos desde express');
});

app.use(cors({
  domains: '*',
  methods: "*"
}));


app.get('/hello', function (req, res) {
  res.send('Consumiendo un API');
});

app.listen(3000, () => {
  console.log("El servidor está inicializado en el puerto 3000");
});
