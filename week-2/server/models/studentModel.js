const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const student = new Schema({
  firstName: { type: String },
  lastName: { type: String },
  email: { type: String },
  address: { type: String }
});

module.exports = mongoose.model('student', student);