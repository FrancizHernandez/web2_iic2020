const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const user = new Schema({
  firstname: { type: String },
  lastname: { type: String },
  user: { type: String },
  password: { type: String, select: false }

});

module.exports = mongoose.model('user', user);