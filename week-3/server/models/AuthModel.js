const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const auth = new Schema({  // session
  username: { type: String },
  sessionID: { type: String },
  started: { type: Date, default: Date.now() }
});

module.exports = mongoose.model('auth', auth);