const User = require("../models/UserModel");
const crypto = require('crypto');
algorithm = 'aes-256-ctr',
  pass = 'd6F3Efeq';
/**
 * Creates a task
 *
 * @param {*} req
 * @param {*} res
 */
const userPost = (req, res) => {
  var user = new User();

  user.firstname = req.body.firstname;
  user.lastname = req.body.lastname;
  user.username = req.body.username;
  user.password = req.body.password;
  let pass = encriptar(user.password);
  if (user.firstname && user.lastname && user.username && pass) {
    user.save(function (err) {

      if (err) {
        res.status(422);
        console.log('error while saving the user', err)
        res.json({
          error: 'There was an error saving the user'
        });
      }

      res.status(201);//CREATED
      res.header({
        'location': `http://localhost:3000/api/user/?id=${user.id}`
      });
      res.json(user);
    });
  } else {
    res.status(422);
    console.log('error while saving the user')
    res.json({
      error: 'No valid data provided for user'
    });
  }
};


function encriptar(text) {
  var cipher = crypto.createCipher(algorithm, pass)
  var crypted = cipher.update(text, 'utf8', 'hex')
  crypted += cipher.final('hex');
  return crypted;
}
/**
 * Get all tasks
 *
 * @param {*} req
 * @param {*} res
 */
const userGet = (req, res) => {
  // if an specific task is required
  if (req.query && req.query.id) {
    User.findById(req.query.id, function (err, user) {
      if (err) {
        res.status(404);
        console.log('error while queryting the user', err)
        res.json({ error: "User doesnt exist" })
      }
      res.json(user);
    });
  } else {
    // get all tasks
    User.find(function (err, user) {
      if (err) {
        res.status(422);
        res.json({ "error": err });
      }
      res.json(user);
    });

  }
};



module.exports = {
  userGet,
  userPost
}