const Auth = require("../models/AuthModel");
const User = require("../models/UserModel");

/**
 * Creates a task
 *
 * @param {*} req
 * @param {*} res
 */
const authPost = (req, res) => {
  var auth = new Auth();

  auth.username = req.body.username;
  auth.sessionID = req.body.sessionID;
  console.log(auth.username);
  console.log(auth.sessionID);
  if (auth.username && auth.sessionID) {
    auth.save(function (err) {
      if (err) {
        res.status(422);
        console.log('error while saving the auth', err)
        res.json({
          error: 'There was an error saving the auth'
        });
      }
      res.status(201);//CREATED
      res.header({
        'location': `http://localhost:3000/api/auth/?id=${auth.id}`
      });
      res.json(auth);
    });
  } else {
    res.status(422);
    console.log('error while saving the auth')
    res.json({
      error: 'No valid data provided for auth'
    });
  }
};

/**
 * Get all tasks
 *
 * @param {*} req
 * @param {*} res
 */
const authGet = (req, res) => {
  // if an specific task is required
  let sessionID = req.query.sessionID;

  if (req.query && sessionID) {
    Auth.findOne({ sessionID: sessionID }, function (err, auth) {
      if (err) {
        res.status(404);
        console.log('error while queryting the auth', err)
        res.json({ error: "Session doesnt exist" })
      }
      User.findById(sessionID, function (err, user) {
        if (err) {
          res.status(404);
          console.log('error while queryting the auth', err)
          res.json({ error: "Session doesnt exist" })
        }
        res.json(user);
      });

    });

    /*Auth.findById(req.query.id, function (err, auth) {
      if (err) {
        res.status(404);
        console.log('error while queryting the auth', err)
        res.json({ error: "Session doesnt exist" })
      }
      User.populate(auth, { path: "sessionID" }, function (err, auth) {
        if (err) {
          res.status(404);
          console.log('error while queryting the auth', err)
          res.json({ error: "User doesnt exist" })
        }
        res.json(auth);
      });
    });*/
  } else {
    // get all tasks
    Auth.find(function (err, auth) {
      if (err) {
        res.status(422);
        res.json({ "error": err });
      }
      res.json(auth);
    });

  }
};




/*const getSessiones = (req, res) => {
  var user = new User();
  if (req.query && req.query.id) {
    Auth.find().populate({ patch: 'sessionID' }).exec((err, auth) => {
      if (err) {
        res.status(500).send({ message: 'Error de conexión a la BD' });
      } else {
        if (!auth) {
          res.status(404).send({ message: 'No existe la session' });
        } else {
          User.populate(auth, { path: 'id' }, (err, user) => {

            if (err) {
              res.status(500).send({ message: 'Error en la peticiona la BD' });
            } else {
              res.status(200).send(auth);
            }
          });
        }
      }
    });
  }
}*/
module.exports = {
  authGet,
  authPost
}