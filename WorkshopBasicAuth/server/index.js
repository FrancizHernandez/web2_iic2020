const express = require('express');
const app = express();
// database connection
const mongoose = require("mongoose");
const db = mongoose.connect("mongodb://127.0.0.1:27017/todo-api");

const {
  taskPatch,
  taskPost,
  taskGet,
  taskDelete
} = require("./controllers/taskController.js");


const {
  studentPost,
  studentGet,
  studentDelete,
  studentPatch
} = require("./controllers/studentController.js");

const {
  userGet,
  userPost
} = require("./controllers/userController.js");
// parser for the request body (required for the POST and PUT methods)
const bodyParser = require("body-parser");
app.use(bodyParser.json());

const {
  base64decode
} = require('nodejs-base64');
const User = require("./models/UserModel.js");
//basic authentication
app.use(function (req, res, next) {

  app.use(function (req, res, next) {
    if (req.headers["authorization"]) {
      // Basic VVROOlBhc3N3b3JkMQ==

      const authBase64 = req.headers['authorization'].split(' ');
      console.log('authBase64:', authBase64);
      const userPass = base64decode(authBase64[1]);
      console.log('userPass:', userPass);
      const user = userPass.split(':')[0];
      const password = userPass.split(':')[1];

      if (user === 'admin' && password == '1234') {
        // saveSession('admin');
        next();
        return;
      }
    }
    res.status(401);
    res.send({
      error: "Unauthorized"
    });
  });
  res.status(401);
  res.send({
    error: "Unauthorized"
  });
});

// check for cors
const cors = require("cors");
app.use(cors({
  domains: '*',
  methods: "*"
}));



// listen to the task request
app.get("/api/tasks", taskGet);
app.post("/api/tasks", taskPost);
app.patch("/api/tasks", taskPatch);
app.put("/api/tasks", taskPatch);
app.delete("/api/tasks", taskDelete);


app.get("/api/student", studentGet);
app.post("/api/student", studentPost);
app.put("/api/tasks", studentPatch);
app.delete("/api/student", studentDelete);

app.get("/api/user", userGet);
app.post("/api/user", userPost);

app.listen(4000, () => { console.log('Server on port 4000') });