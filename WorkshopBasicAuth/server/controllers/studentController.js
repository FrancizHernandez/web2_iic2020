const Student = require("../models/studentModel");

/**
 * Creates a student
 *
 * @param {*} req
 * @param {*} res
 */
const studentPost = (req, res) => {
  var student = new Student();

  student.firstname = req.body.firstname;
  student.lastname = req.body.lastname;
  student.email = req.body.email;
  student.address = req.body.address;

  if (student.firstname && student.lastname && student.email && student.address) {
    student.save(function (err) {
      if (err) {
        res.status(422);
        console.log('error while saving the student', err)
        res.json({
          error: 'There was an error saving the student'
        });
      }
      res.status(201);//CREATED
      res.header({
        'location': `http://localhost:3000/api/student/?id=${student.id}`
      });
      res.json(student);
    });
  } else {
    res.status(422);
    console.log('error while saving the student')
    res.json({
      error: 'No valid data provided for student'
    });
  }
};

/**
 * Get all students
 *
 * @param {*} req
 * @param {*} res
 */
const studentGet = (req, res) => {
  // if an specific student is required
  if (req.query && req.query.id) {
    Student.findById(req.query.id, function (err, student) {
      if (err) {
        res.status(404);
        console.log('error while queryting the student', err)
        res.json({ error: "Student doesnt exist" })
      }
      res.json(student);
    });
  } else {
    // get all students
    Student.find(function (err, student) {
      if (err) {
        res.status(422);
        res.json({ "error": err });
      }
      res.json(student);
    });

  }
};

/**
 * Updates a student
 *
 * @param {*} req
 * @param {*} res
 */
const studentPatch = (req, res) => {
  // get student by id
  if (req.query && req.query.id) {
    student.findById(req.query.id, function (err, student) {
      if (err) {
        res.status(404);
        console.log('error while queryting the student', err)
        res.json({ error: "student doesnt exist" })
      }

      // update the student object (patch)
      student.firstname = req.body.firstname ? req.body.firstname : student.firstname;
      student.lastname = req.body.lastname ? req.body.lastname : student.lastname;
      student.email = req.body.email ? req.body.lastname : student.email;
      student.address = req.body.address ? req.body.address : student.address;
      // update the student object (put)
      // student.title = req.body.title
      // student.detail = req.body.detail

      student.save(function (err) {
        if (err) {
          res.status(422);
          console.log('error while saving the student', err)
          res.json({
            error: 'There was an error saving the student'
          });
        }
        res.status(200); // OK
        res.json(student);
      });
    });
  } else {
    res.status(404);
    res.json({ error: "student doesnt exist" })
  }
};

const studentDelete = (req, res) => {
  var id = req.query.id
  if (req.query && req.query.id) {
    Student.findByIdAndRemove(req.query.id, function (err, student) {
      if (err) {
        return res.json(500, {
          message: 'We didnt find student'
        })
      }
      res.status(204);// para borrar
      return res.json(student)
    })
  }
  else {
    res.status(404);
    res.json({ error: "student doesnt exist" })
  }
};


module.exports = {
  studentGet,
  studentPost,
  studentPatch,
  studentDelete
}